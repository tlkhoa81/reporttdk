# ReportTDK

Thư viện tạo báo cáo excel từ template excel

Code to create report:
      ResultSet rs= wks.getResultSet(strSQL);

      Date dt= new Date();
      SimpleDateFormat formatter= new SimpleDateFormat ("dd-MM-yyyy-");

      String strDate= formatter.format(dt)  + String.valueOf(dt.getTime());
      String strFileName = "Customer" + strDate   + ".xls";

      String strReportPath = "Reports\\" + strFileName;
      String strTemplatePath = "Reports\\Template\\Customer.xls";

      Report report= new Report(rs, strTemplatePath,strPath);

      Date dt= new Date();
      SimpleDateFormat formatter= new SimpleDateFormat ("dd/MM/yyyy");
      String strReportDate = formatter.format(dt);
      
      report.setParameter("$ReportDate", strReportDate);
      report.fillDataToExcel();

